EESchema Schematic File Version 4
LIBS:KICAD_Platine1.0-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "reset_Cirquit"
Date "2018-10-30"
Rev "V 1.0"
Comp "CURE Mannheim"
Comment1 ""
Comment2 "Maximilian Majeske"
Comment3 "Author: William Großmann"
Comment4 "Brake System Plausability Deveice"
$EndDescr
$Comp
L Transistor_FET:BS250 Q?
U 1 1 5BD9A8CD
P 5450 3550
AR Path="/5BD9A8CD" Ref="Q?"  Part="1" 
AR Path="/5BD98D69/5BD9A8CD" Ref="Q3"  Part="1" 
F 0 "Q3" H 5655 3504 50  0000 L CNN
F 1 "BS250" H 5655 3595 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5650 3475 50  0001 L CIN
F 3 "http://www.vishay.com/docs/70209/70209.pdf" H 5450 3550 50  0001 L CNN
	1    5450 3550
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BD9A8D4
P 6350 3750
AR Path="/5BD9A8D4" Ref="R?"  Part="1" 
AR Path="/5BD98D69/5BD9A8D4" Ref="R30"  Part="1" 
F 0 "R30" V 6154 3750 50  0000 C CNN
F 1 "47k" V 6245 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6350 3750 50  0001 C CNN
F 3 "~" H 6350 3750 50  0001 C CNN
	1    6350 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BD9A8DB
P 7100 3400
AR Path="/5BD9A8DB" Ref="R?"  Part="1" 
AR Path="/5BD98D69/5BD9A8DB" Ref="R32"  Part="1" 
F 0 "R32" H 7041 3354 50  0000 R CNN
F 1 "10k" H 7041 3445 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7100 3400 50  0001 C CNN
F 3 "~" H 7100 3400 50  0001 C CNN
	1    7100 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BD9A8E2
P 7100 4250
AR Path="/5BD9A8E2" Ref="R?"  Part="1" 
AR Path="/5BD98D69/5BD9A8E2" Ref="R33"  Part="1" 
F 0 "R33" H 7041 4204 50  0000 R CNN
F 1 "10k" H 7041 4295 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7100 4250 50  0001 C CNN
F 3 "~" H 7100 4250 50  0001 C CNN
	1    7100 4250
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5BD9A8E9
P 6650 4300
AR Path="/5BD9A8E9" Ref="C?"  Part="1" 
AR Path="/5BD98D69/5BD9A8E9" Ref="C12"  Part="1" 
F 0 "C12" H 6742 4346 50  0000 L CNN
F 1 "470u" H 6742 4255 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.3" H 6650 4300 50  0001 C CNN
F 3 "~" H 6650 4300 50  0001 C CNN
	1    6650 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BD9A8F0
P 6350 4050
AR Path="/5BD9A8F0" Ref="R?"  Part="1" 
AR Path="/5BD98D69/5BD9A8F0" Ref="R31"  Part="1" 
F 0 "R31" V 6154 4050 50  0000 C CNN
F 1 "10" V 6245 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6350 4050 50  0001 C CNN
F 3 "~" H 6350 4050 50  0001 C CNN
	1    6350 4050
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:BS170 Q?
U 1 1 5BD9A8F7
P 5450 4250
AR Path="/5BD9A8F7" Ref="Q?"  Part="1" 
AR Path="/5BD98D69/5BD9A8F7" Ref="Q4"  Part="1" 
F 0 "Q4" H 5655 4296 50  0000 L CNN
F 1 "BS170" H 5655 4205 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5650 4175 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BS/BS170.pdf" H 5450 4250 50  0001 L CNN
	1    5450 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BD9A904
P 7100 4450
AR Path="/5BD9A904" Ref="#PWR?"  Part="1" 
AR Path="/5BD98D69/5BD9A904" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 7100 4200 50  0001 C CNN
F 1 "GND" H 7105 4277 50  0000 C CNN
F 2 "" H 7100 4450 50  0001 C CNN
F 3 "" H 7100 4450 50  0001 C CNN
	1    7100 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3750 6650 3750
Wire Wire Line
	6450 4050 6650 4050
Connection ~ 6650 4050
Wire Wire Line
	6650 4050 6650 3750
Wire Wire Line
	6250 4050 5550 4050
Wire Wire Line
	6650 4400 6650 4450
Wire Wire Line
	6650 4050 6650 4200
Wire Wire Line
	5550 3750 6250 3750
Wire Wire Line
	5550 3350 5550 3200
Wire Wire Line
	5550 3200 7100 3200
Wire Wire Line
	7100 3200 7100 3300
Wire Wire Line
	4900 3550 5250 3550
Wire Wire Line
	4900 4250 5250 4250
Text GLabel 8200 3850 2    50   Input ~ 0
C_reset
$Comp
L Comparator:LM2903 U?
U 2 1 5BD9A923
P 7700 3850
AR Path="/5BD9A923" Ref="U?"  Part="2" 
AR Path="/5BD98D69/5BD9A923" Ref="U9"  Part="2" 
F 0 "U9" H 7700 4217 50  0000 C CNN
F 1 "LM2903" H 7700 4126 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7700 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 7700 3850 50  0001 C CNN
	2    7700 3850
	1    0    0    -1  
$EndComp
Text Notes 4300 3500 0    50   ~ 0
CD4081B
$Comp
L 4xxx:4081 U?
U 2 1 5BD9A92C
P 3900 3750
AR Path="/5BD9A92C" Ref="U?"  Part="2" 
AR Path="/5BD98D69/5BD9A92C" Ref="U10"  Part="2" 
F 0 "U10" H 3900 4075 50  0000 C CNN
F 1 "4081" H 3900 3984 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3900 3750 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3900 3750 50  0001 C CNN
	2    3900 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3550 4900 3950
Connection ~ 4900 3950
Wire Wire Line
	4900 3950 4900 4250
$Comp
L 4xxx:4011 U?
U 1 1 5BD9A936
P 4600 3950
AR Path="/5BD9A936" Ref="U?"  Part="1" 
AR Path="/5BD98D69/5BD9A936" Ref="U11"  Part="1" 
F 0 "U11" H 4600 4275 50  0000 C CNN
F 1 "4011" H 4600 4184 50  0000 C CNN
F 2 "Package_SO:SSOP-14_5.3x6.2mm_P0.65mm" H 4600 3950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 4600 3950 50  0001 C CNN
	1    4600 3950
	1    0    0    -1  
$EndComp
Text Notes 5650 4400 0    50   ~ 0
R=5ohm\n
Text Notes 5650 3700 0    50   ~ 0
R_on=20 Ohm\n
Text GLabel 3500 3650 0    50   Output ~ 0
C_Power_high
Text GLabel 3500 3850 0    50   Output ~ 0
C_Brake_high
Text GLabel 3500 4050 0    50   Output ~ 0
C_openCirquit
Wire Wire Line
	3500 3650 3600 3650
Wire Wire Line
	3500 3850 3600 3850
Wire Wire Line
	4200 3750 4250 3750
Wire Wire Line
	4250 3750 4250 3850
Wire Wire Line
	4250 3850 4300 3850
Wire Wire Line
	3500 4050 4300 4050
Text GLabel 7100 3150 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	7100 3150 7100 3200
Connection ~ 7100 3200
Wire Wire Line
	7100 3500 7100 3950
Wire Wire Line
	7100 4350 7100 4450
Wire Wire Line
	7400 3950 7100 3950
Connection ~ 7100 3950
Wire Wire Line
	7100 3950 7100 4150
Wire Wire Line
	7400 3750 6650 3750
Connection ~ 6650 3750
$Comp
L power:GND #PWR?
U 1 1 5BE26EAA
P 6650 4450
AR Path="/5BE26EAA" Ref="#PWR?"  Part="1" 
AR Path="/5BD98D69/5BE26EAA" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 6650 4200 50  0001 C CNN
F 1 "GND" H 6655 4277 50  0000 C CNN
F 2 "" H 6650 4450 50  0001 C CNN
F 3 "" H 6650 4450 50  0001 C CNN
	1    6650 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BE26EC9
P 5550 4450
AR Path="/5BE26EC9" Ref="#PWR?"  Part="1" 
AR Path="/5BD98D69/5BE26EC9" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 5550 4200 50  0001 C CNN
F 1 "GND" H 5555 4277 50  0000 C CNN
F 2 "" H 5550 4450 50  0001 C CNN
F 3 "" H 5550 4450 50  0001 C CNN
	1    5550 4450
	1    0    0    -1  
$EndComp
Text Notes 9950 3850 0    50   ~ 0
Versorgung auf Seite 4/6
Wire Wire Line
	8000 3850 8050 3850
$Comp
L Device:R_Small R?
U 1 1 5BEB3D8A
P 8050 3400
AR Path="/5BEB3D8A" Ref="R?"  Part="1" 
AR Path="/5BD98D69/5BEB3D8A" Ref="R34"  Part="1" 
F 0 "R34" H 7991 3354 50  0000 R CNN
F 1 "10k" H 7991 3445 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8050 3400 50  0001 C CNN
F 3 "~" H 8050 3400 50  0001 C CNN
	1    8050 3400
	-1   0    0    1   
$EndComp
Text GLabel 8050 3150 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	8050 3150 8050 3300
Wire Wire Line
	8050 3500 8050 3850
Connection ~ 8050 3850
Wire Wire Line
	8050 3850 8200 3850
Text Notes 3600 3350 0    50   ~ 0
AND statt NAND
Text Notes 3700 4450 0    50   ~ 0
Beide Gatter getauscht\n(Signale - AND\nopen Cirquit - NAND)
$Comp
L 4xxx:4081 U10
U 5 1 5BF20C34
P 10050 2500
F 0 "U10" H 10280 2546 50  0000 L CNN
F 1 "4081" H 10280 2455 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 10050 2500 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 10050 2500 50  0001 C CNN
	5    10050 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BF20CC0
P 10050 3100
AR Path="/5BF20CC0" Ref="#PWR?"  Part="1" 
AR Path="/5BD98D69/5BF20CC0" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 10050 2850 50  0001 C CNN
F 1 "GND" H 10055 2927 50  0000 C CNN
F 2 "" H 10050 3100 50  0001 C CNN
F 3 "" H 10050 3100 50  0001 C CNN
	1    10050 3100
	1    0    0    -1  
$EndComp
Text GLabel 10050 1850 1    50   Output ~ 0
Vcc_5V
$Comp
L Device:C C13
U 1 1 5BF20D14
P 9500 2500
F 0 "C13" H 9615 2546 50  0000 L CNN
F 1 "100n" H 9615 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9538 2350 50  0001 C CNN
F 3 "~" H 9500 2500 50  0001 C CNN
	1    9500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 1850 10050 1950
Wire Wire Line
	10050 3000 10050 3050
Wire Wire Line
	9500 2650 9500 3050
Wire Wire Line
	9500 3050 10050 3050
Connection ~ 10050 3050
Wire Wire Line
	10050 3050 10050 3100
Wire Wire Line
	9500 1950 10050 1950
Connection ~ 10050 1950
Wire Wire Line
	10050 1950 10050 2000
Wire Wire Line
	9500 1950 9500 2350
$Comp
L 4xxx:4011 U11
U 5 1 5BF687BC
P 10050 4950
F 0 "U11" H 10280 4996 50  0000 L CNN
F 1 "4011" H 10280 4905 50  0000 L CNN
F 2 "Package_SO:SSOP-14_5.3x6.2mm_P0.65mm" H 10050 4950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 10050 4950 50  0001 C CNN
	5    10050 4950
	1    0    0    -1  
$EndComp
Text GLabel 10050 4400 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR?
U 1 1 5BF68918
P 10050 5500
AR Path="/5BF68918" Ref="#PWR?"  Part="1" 
AR Path="/5BD98D69/5BF68918" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 10050 5250 50  0001 C CNN
F 1 "GND" H 10055 5327 50  0000 C CNN
F 2 "" H 10050 5500 50  0001 C CNN
F 3 "" H 10050 5500 50  0001 C CNN
	1    10050 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5BF68977
P 9500 4950
F 0 "C17" H 9615 4996 50  0000 L CNN
F 1 "100n" H 9615 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9538 4800 50  0001 C CNN
F 3 "~" H 9500 4950 50  0001 C CNN
	1    9500 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 4400 10050 4450
Wire Wire Line
	10050 5500 10050 5450
Wire Wire Line
	9500 4800 9500 4450
Wire Wire Line
	9500 4450 10050 4450
Connection ~ 10050 4450
Wire Wire Line
	9500 5100 9500 5500
Wire Wire Line
	9500 5500 10050 5500
Connection ~ 10050 5500
$Comp
L 4xxx:4011 U11
U 2 1 5C08C43B
P 3950 5050
F 0 "U11" H 3950 5375 50  0000 C CNN
F 1 "4011" H 3950 5284 50  0000 C CNN
F 2 "Package_SO:SSOP-14_5.3x6.2mm_P0.65mm" H 3950 5050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 3950 5050 50  0001 C CNN
	2    3950 5050
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4011 U11
U 3 1 5C08C4C4
P 3950 5600
F 0 "U11" H 3950 5925 50  0000 C CNN
F 1 "4011" H 3950 5834 50  0000 C CNN
F 2 "Package_SO:SSOP-14_5.3x6.2mm_P0.65mm" H 3950 5600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 3950 5600 50  0001 C CNN
	3    3950 5600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4011 U11
U 4 1 5C08C54B
P 3950 6150
F 0 "U11" H 3950 6475 50  0000 C CNN
F 1 "4011" H 3950 6384 50  0000 C CNN
F 2 "Package_SO:SSOP-14_5.3x6.2mm_P0.65mm" H 3950 6150 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 3950 6150 50  0001 C CNN
	4    3950 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5C08C61B
P 3550 6400
F 0 "#PWR0106" H 3550 6150 50  0001 C CNN
F 1 "GND" H 3555 6227 50  0000 C CNN
F 2 "" H 3550 6400 50  0001 C CNN
F 3 "" H 3550 6400 50  0001 C CNN
	1    3550 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4950 3550 4950
Wire Wire Line
	3550 4950 3550 5150
Wire Wire Line
	3650 5150 3550 5150
Connection ~ 3550 5150
Wire Wire Line
	3550 5150 3550 5500
Wire Wire Line
	3650 5500 3550 5500
Connection ~ 3550 5500
Wire Wire Line
	3550 5500 3550 5700
Wire Wire Line
	3650 5700 3550 5700
Connection ~ 3550 5700
Wire Wire Line
	3550 5700 3550 6050
Wire Wire Line
	3650 6050 3550 6050
Connection ~ 3550 6050
Wire Wire Line
	3550 6050 3550 6250
Wire Wire Line
	3650 6250 3550 6250
Connection ~ 3550 6250
Wire Wire Line
	3550 6250 3550 6400
$Comp
L 4xxx:4081 U10
U 1 1 5C09088D
P 2400 5050
F 0 "U10" H 2400 5375 50  0000 C CNN
F 1 "4081" H 2400 5284 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2400 5050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 2400 5050 50  0001 C CNN
	1    2400 5050
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U10
U 3 1 5C09091E
P 2400 5450
F 0 "U10" H 2400 5775 50  0000 C CNN
F 1 "4081" H 2400 5684 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2400 5450 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 2400 5450 50  0001 C CNN
	3    2400 5450
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U10
U 4 1 5C090995
P 2400 5850
F 0 "U10" H 2400 6175 50  0000 C CNN
F 1 "4081" H 2400 6084 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2400 5850 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 2400 5850 50  0001 C CNN
	4    2400 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5C090A52
P 2000 6400
F 0 "#PWR0107" H 2000 6150 50  0001 C CNN
F 1 "GND" H 2005 6227 50  0000 C CNN
F 2 "" H 2000 6400 50  0001 C CNN
F 3 "" H 2000 6400 50  0001 C CNN
	1    2000 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4950 2100 4950
Wire Wire Line
	2000 5150 2100 5150
Connection ~ 2000 5150
Wire Wire Line
	2000 5150 2000 4950
Wire Wire Line
	2000 5350 2100 5350
Connection ~ 2000 5350
Wire Wire Line
	2000 5350 2000 5150
Wire Wire Line
	2000 5550 2100 5550
Connection ~ 2000 5550
Wire Wire Line
	2000 5550 2000 5350
Wire Wire Line
	2000 5750 2100 5750
Wire Wire Line
	2000 5550 2000 5750
Connection ~ 2000 5750
Wire Wire Line
	2000 5750 2000 5950
Wire Wire Line
	2000 5950 2100 5950
Connection ~ 2000 5950
Wire Wire Line
	2000 5950 2000 6400
$EndSCHEMATC
