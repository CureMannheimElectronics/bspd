EESchema Schematic File Version 4
LIBS:KICAD_Platine1.0-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "Triger Cirquit"
Date "2018-10-30"
Rev "V 1.0"
Comp "CURE Mannheim"
Comment1 ""
Comment2 "Maximilian Majeske"
Comment3 "Author: William Großmann"
Comment4 "Brake System Plausability Deveice"
$EndDescr
$Comp
L Device:R_Small R?
U 1 1 5BD9A17B
P 5800 3700
AR Path="/5BD9A17B" Ref="R?"  Part="1" 
AR Path="/5BD9871A/5BD9A17B" Ref="R25"  Part="1" 
F 0 "R25" V 5604 3700 50  0000 C CNN
F 1 "56k" V 5695 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5800 3700 50  0001 C CNN
F 3 "~" H 5800 3700 50  0001 C CNN
	1    5800 3700
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:BS170 Q?
U 1 1 5BD9A182
P 4900 4250
AR Path="/5BD9A182" Ref="Q?"  Part="1" 
AR Path="/5BD9871A/5BD9A182" Ref="Q2"  Part="1" 
F 0 "Q2" H 5105 4296 50  0000 L CNN
F 1 "BS170" H 5105 4205 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5100 4175 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BS/BS170.pdf" H 4900 4250 50  0001 L CNN
	1    4900 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BD9A189
P 5800 4000
AR Path="/5BD9A189" Ref="R?"  Part="1" 
AR Path="/5BD9871A/5BD9A189" Ref="R26"  Part="1" 
F 0 "R26" V 5604 4000 50  0000 C CNN
F 1 "47" V 5695 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5800 4000 50  0001 C CNN
F 3 "~" H 5800 4000 50  0001 C CNN
	1    5800 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BD9A190
P 6550 3350
AR Path="/5BD9A190" Ref="R?"  Part="1" 
AR Path="/5BD9871A/5BD9A190" Ref="R27"  Part="1" 
F 0 "R27" H 6491 3304 50  0000 R CNN
F 1 "10k" H 6491 3395 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6550 3350 50  0001 C CNN
F 3 "~" H 6550 3350 50  0001 C CNN
	1    6550 3350
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BD9A197
P 6550 4250
AR Path="/5BD9A197" Ref="R?"  Part="1" 
AR Path="/5BD9871A/5BD9A197" Ref="R28"  Part="1" 
F 0 "R28" H 6491 4204 50  0000 R CNN
F 1 "10k" H 6491 4295 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6550 4250 50  0001 C CNN
F 3 "~" H 6550 4250 50  0001 C CNN
	1    6550 4250
	-1   0    0    1   
$EndComp
$Comp
L Comparator:LM2903 U?
U 1 1 5BD9A19E
P 7050 3800
AR Path="/5BD9A19E" Ref="U?"  Part="1" 
AR Path="/5BD9871A/5BD9A19E" Ref="U9"  Part="1" 
F 0 "U9" H 7050 4167 50  0000 C CNN
F 1 "LM2903" H 7050 4076 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7050 3800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 7050 3800 50  0001 C CNN
	1    7050 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5BD9A1A5
P 6100 4200
AR Path="/5BD9A1A5" Ref="C?"  Part="1" 
AR Path="/5BD9871A/5BD9A1A5" Ref="C9"  Part="1" 
F 0 "C9" H 6192 4246 50  0000 L CNN
F 1 "10u" H 6192 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 4200 50  0001 C CNN
F 3 "~" H 6100 4200 50  0001 C CNN
	1    6100 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BD9A1AC
P 6550 4450
AR Path="/5BD9A1AC" Ref="#PWR?"  Part="1" 
AR Path="/5BD9871A/5BD9A1AC" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 6550 4200 50  0001 C CNN
F 1 "GND" H 6555 4277 50  0000 C CNN
F 2 "" H 6550 4450 50  0001 C CNN
F 3 "" H 6550 4450 50  0001 C CNN
	1    6550 4450
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:BS250 Q?
U 1 1 5BD9A1B2
P 4900 3450
AR Path="/5BD9A1B2" Ref="Q?"  Part="1" 
AR Path="/5BD9871A/5BD9A1B2" Ref="Q1"  Part="1" 
F 0 "Q1" H 5105 3404 50  0000 L CNN
F 1 "BS250" H 5105 3495 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5100 3375 50  0001 L CIN
F 3 "http://www.vishay.com/docs/70209/70209.pdf" H 4900 3450 50  0001 L CNN
	1    4900 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	6100 3700 6100 4000
Wire Wire Line
	6100 3700 5900 3700
Wire Wire Line
	6100 4000 5900 4000
Connection ~ 6100 4000
Wire Wire Line
	6100 4000 6100 4100
Wire Wire Line
	5700 4000 5000 4000
Wire Wire Line
	5000 4000 5000 4050
Wire Wire Line
	5000 3650 5000 3700
Wire Wire Line
	5000 3700 5700 3700
Wire Wire Line
	4700 4250 4350 4250
Wire Wire Line
	4350 3450 4700 3450
Wire Wire Line
	6100 4300 6100 4450
Wire Wire Line
	5000 3250 5000 3150
Wire Wire Line
	5000 3150 6550 3150
Wire Wire Line
	6550 3150 6550 3250
Wire Wire Line
	4350 3450 4350 3850
Connection ~ 4350 3850
Wire Wire Line
	4350 3850 4350 4250
Text GLabel 3500 3700 0    50   Output ~ 0
C_Power_high
Text GLabel 3500 4000 0    50   Output ~ 0
C_Brake_high
Text GLabel 7450 3800 2    50   Input ~ 0
C_triggerBSPD
Wire Wire Line
	7450 3800 7400 3800
Text GLabel 6550 3100 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	6550 3100 6550 3150
Connection ~ 6550 3150
$Comp
L Comparator:LM2903 U9
U 3 1 5BE1F383
P 9900 2800
F 0 "U9" H 9858 2846 50  0000 L CNN
F 1 "LM2903" H 9858 2755 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9900 2800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 9900 2800 50  0001 C CNN
	3    9900 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5BE1F506
P 9400 2800
F 0 "C10" H 9515 2846 50  0000 L CNN
F 1 "100n" H 9515 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9438 2650 50  0001 C CNN
F 3 "~" H 9400 2800 50  0001 C CNN
	1    9400 2800
	1    0    0    -1  
$EndComp
Text GLabel 9800 2400 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR?
U 1 1 5BE1F992
P 9800 3200
AR Path="/5BE1F992" Ref="#PWR?"  Part="1" 
AR Path="/5BD9871A/5BE1F992" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 9800 2950 50  0001 C CNN
F 1 "GND" H 9805 3027 50  0000 C CNN
F 2 "" H 9800 3200 50  0001 C CNN
F 3 "" H 9800 3200 50  0001 C CNN
	1    9800 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 2400 9800 2450
Wire Wire Line
	9800 3100 9800 3150
Wire Wire Line
	9400 2950 9400 3150
Wire Wire Line
	9400 3150 9800 3150
Connection ~ 9800 3150
Wire Wire Line
	9800 3150 9800 3200
Wire Wire Line
	9800 2450 9400 2450
Wire Wire Line
	9400 2450 9400 2650
Connection ~ 9800 2450
Wire Wire Line
	9800 2450 9800 2500
Wire Wire Line
	6550 3450 6550 3900
Wire Wire Line
	6550 4350 6550 4450
Wire Wire Line
	6750 3900 6550 3900
Connection ~ 6550 3900
Wire Wire Line
	6550 3900 6550 4150
Wire Wire Line
	6750 3700 6100 3700
Connection ~ 6100 3700
$Comp
L power:GND #PWR?
U 1 1 5BE26DD9
P 6100 4450
AR Path="/5BE26DD9" Ref="#PWR?"  Part="1" 
AR Path="/5BD9871A/5BE26DD9" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 6100 4200 50  0001 C CNN
F 1 "GND" H 6105 4277 50  0000 C CNN
F 2 "" H 6100 4450 50  0001 C CNN
F 3 "" H 6100 4450 50  0001 C CNN
	1    6100 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BE26DFA
P 5000 4450
AR Path="/5BE26DFA" Ref="#PWR?"  Part="1" 
AR Path="/5BD9871A/5BE26DFA" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 5000 4200 50  0001 C CNN
F 1 "GND" H 5005 4277 50  0000 C CNN
F 2 "" H 5000 4450 50  0001 C CNN
F 3 "" H 5000 4450 50  0001 C CNN
	1    5000 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5BEB3091
P 7400 3350
AR Path="/5BEB3091" Ref="R?"  Part="1" 
AR Path="/5BD9871A/5BEB3091" Ref="R29"  Part="1" 
F 0 "R29" H 7341 3304 50  0000 R CNN
F 1 "10k" H 7341 3395 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7400 3350 50  0001 C CNN
F 3 "~" H 7400 3350 50  0001 C CNN
	1    7400 3350
	-1   0    0    1   
$EndComp
Text GLabel 7400 3050 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	7400 3050 7400 3250
Wire Wire Line
	7400 3450 7400 3800
Connection ~ 7400 3800
Wire Wire Line
	7400 3800 7350 3800
Text Notes 3800 3400 0    50   ~ 0
OR statt AND\n
$Comp
L 4xxx:4072 U2
U 2 1 5BF1C2E0
P 4050 3850
F 0 "U2" H 4050 4225 50  0000 C CNN
F 1 "4072" H 4050 4134 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4050 3850 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 4050 3850 50  0001 C CNN
	2    4050 3850
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4072 U2
U 3 1 5BF1C34B
P 9900 5000
F 0 "U2" H 10130 5046 50  0000 L CNN
F 1 "4072" H 10130 4955 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 9900 5000 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 9900 5000 50  0001 C CNN
	3    9900 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3700 3750 3700
Wire Wire Line
	3500 4000 3750 4000
$Comp
L Device:C C11
U 1 1 5BF1D1E5
P 9400 5050
F 0 "C11" H 9515 5096 50  0000 L CNN
F 1 "100n" H 9515 5005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9438 4900 50  0001 C CNN
F 3 "~" H 9400 5050 50  0001 C CNN
	1    9400 5050
	1    0    0    -1  
$EndComp
Text GLabel 9900 4400 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR?
U 1 1 5BF1D1ED
P 9900 5600
AR Path="/5BF1D1ED" Ref="#PWR?"  Part="1" 
AR Path="/5BD9871A/5BF1D1ED" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 9900 5350 50  0001 C CNN
F 1 "GND" H 9905 5427 50  0000 C CNN
F 2 "" H 9900 5600 50  0001 C CNN
F 3 "" H 9900 5600 50  0001 C CNN
	1    9900 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 4900 9400 4500
Wire Wire Line
	9400 4500 9900 4500
Wire Wire Line
	9900 4400 9900 4500
Connection ~ 9900 4500
Wire Wire Line
	9900 5600 9900 5550
Wire Wire Line
	9900 5550 9400 5550
Wire Wire Line
	9400 5550 9400 5200
Connection ~ 9900 5550
Wire Wire Line
	9900 5550 9900 5500
$Comp
L power:GND #PWR0104
U 1 1 5C0796BF
P 3700 4150
F 0 "#PWR0104" H 3700 3900 50  0001 C CNN
F 1 "GND" H 3705 3977 50  0000 C CNN
F 2 "" H 3700 4150 50  0001 C CNN
F 3 "" H 3700 4150 50  0001 C CNN
	1    3700 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3900 3700 3900
Wire Wire Line
	3700 3900 3700 4150
Wire Wire Line
	3750 3800 3700 3800
Wire Wire Line
	3700 3800 3700 3900
Connection ~ 3700 3900
$EndSCHEMATC
