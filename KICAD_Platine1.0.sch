EESchema Schematic File Version 4
LIBS:KICAD_Platine1.0-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "BSPD_Main"
Date "2018-10-23"
Rev "V 1.0"
Comp "CURE Mannheim"
Comment1 ""
Comment2 "Maximilian Majeske"
Comment3 "Author: William Großmann"
Comment4 "Brake System Plausability Deveice"
$EndDescr
Text GLabel 4700 4600 2    50   Input ~ 0
C_openCirquit
$Comp
L 4xxx:4072 U2
U 1 1 5BD88F7A
P 2900 4250
F 0 "U2" H 2900 4625 50  0000 C CNN
F 1 "4072" H 2900 4534 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2900 4250 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 2900 4250 50  0001 C CNN
	1    2900 4250
	1    0    0    -1  
$EndComp
$Sheet
S 13250 0    11700 8250
U 5BD8CA1D
F0 "Signals" 50
F1 "Signals.sch" 50
$EndSheet
$Sheet
S 26250 100  11700 8250
U 5BDABB9E
F0 "siganl_brake" 50
F1 "signal_brake.sch" 50
$EndSheet
Text GLabel 3800 5100 3    50   Output ~ 0
C_reset
Text GLabel 1600 3600 0    50   Output ~ 0
C_triggerBSPD
Text GLabel 1500 3950 0    50   Output ~ 0
C_Powersensor_ok
Text GLabel 1500 4300 0    50   Output ~ 0
C_Brakesensor1_ok
$Comp
L 4xxx:4027 U3
U 3 1 5BD8EF83
P 9250 4500
F 0 "U3" H 9480 4546 50  0000 L CNN
F 1 "4027" H 9480 4455 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 9250 4500 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4027bms.pdf" H 9250 4500 50  0001 C CNN
	3    9250 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5BD8F12A
P 9250 5000
F 0 "#PWR04" H 9250 4750 50  0001 C CNN
F 1 "GND" H 9255 4827 50  0000 C CNN
F 2 "" H 9250 5000 50  0001 C CNN
F 3 "" H 9250 5000 50  0001 C CNN
	1    9250 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5BD935C2
P 8650 4550
F 0 "C2" H 8765 4596 50  0000 L CNN
F 1 "100nF" H 8765 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8688 4400 50  0001 C CNN
F 3 "~" H 8650 4550 50  0001 C CNN
	1    8650 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 4400 8650 4050
Wire Wire Line
	8650 4700 8650 4950
$Sheet
S 13400 9200 11700 8250
U 5BD9871A
F0 "triggerCirquit" 50
F1 "triggerCirquit.sch" 50
$EndSheet
$Sheet
S 26150 9200 11650 8250
U 5BD98D69
F0 "resetCirquit" 50
F1 "resetCirquit.sch" 50
$EndSheet
Text GLabel 9250 3900 1    50   Output ~ 0
Vcc_5V
$Sheet
S 300  9300 11700 8200
U 5BD8D0CD
F0 "powersupply" 50
F1 "powersupply.sch" 50
$EndSheet
$Comp
L Connector:DB9_Female_MountingHoles J1
U 1 1 5BEB49D9
P 3400 1950
F 0 "J1" H 3580 1953 50  0000 L CNN
F 1 "DB9_Female_MountingHoles" H 3580 1862 50  0000 L CNN
F 2 "1-106505-2:11065052" H 3400 1950 50  0001 C CNN
F 3 " ~" H 3400 1950 50  0001 C CNN
	1    3400 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5BEB4FD0
P 3100 1650
F 0 "#PWR01" H 3100 1400 50  0001 C CNN
F 1 "GND" H 3105 1477 50  0000 C CNN
F 2 "" H 3100 1650 50  0001 C CNN
F 3 "" H 3100 1650 50  0001 C CNN
	1    3100 1650
	0    1    1    0   
$EndComp
Text GLabel 2800 2250 0    50   Input ~ 0
Vcc_24V
Text GLabel 2800 1550 0    50   Input ~ 0
S_Power
Text GLabel 2800 1750 0    50   Input ~ 0
S_Brake1
Text GLabel 2800 1950 0    50   Input ~ 0
S_Brake2
$Comp
L power:GND #PWR02
U 1 1 5BEB5DED
P 3400 2650
F 0 "#PWR02" H 3400 2400 50  0001 C CNN
F 1 "GND" H 3405 2477 50  0000 C CNN
F 2 "" H 3400 2650 50  0001 C CNN
F 3 "" H 3400 2650 50  0001 C CNN
	1    3400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2550 3400 2650
Text GLabel 2800 2350 0    50   Input ~ 0
ShutDownCirquit_input
Text GLabel 2800 2150 0    50   Output ~ 0
ShutDownCirquit_output
Text GLabel 1500 4650 0    50   Output ~ 0
C_Brakesensor2_ok
Wire Wire Line
	2800 1550 3100 1550
Wire Wire Line
	2800 1750 3100 1750
Wire Wire Line
	2800 1950 3100 1950
Wire Wire Line
	2800 2150 3100 2150
$Comp
L Power_Management:BTS443P U4
U 1 1 5BF3E7C0
P 6700 4600
F 0 "U4" H 6700 4226 50  0000 C CNN
F 1 "BTS443P" H 6700 4135 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-4" H 6700 4200 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Infineon-BTS443P-DS-v01_00-EN.pdf?fileId=5546d4625a888733015aa9afbc5035d5" H 6700 4200 50  0001 C CNN
	1    6700 4600
	1    0    0    -1  
$EndComp
Text GLabel 6700 4100 1    50   Output ~ 0
ShutDownCirquit_input
Text GLabel 7300 4500 2    50   Input ~ 0
ShutDownCirquit_output
Wire Wire Line
	6700 4100 6700 4200
Wire Wire Line
	7100 4500 7100 4600
Wire Wire Line
	7100 4500 7300 4500
Connection ~ 7100 4500
Text GLabel 6250 4600 0    50   Output ~ 0
C_openCirquit
Wire Wire Line
	6250 4600 6300 4600
Text Notes 1250 5000 0    50   ~ 0
hier müssen die ok_Signale invertiert werden\n
Text Notes 750  2000 0    50   ~ 0
Versorgung für Stromsensoren 5V\nSensor 24V\n
$Comp
L 4xxx:4069 U1
U 1 1 5BF48FA0
P 1900 3950
F 0 "U1" H 1900 4267 50  0000 C CNN
F 1 "4069" H 1900 4176 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 1900 3950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 1900 3950 50  0001 C CNN
	1    1900 3950
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U1
U 2 1 5BF4949E
P 1900 4300
F 0 "U1" H 1900 4617 50  0000 C CNN
F 1 "4069" H 1900 4526 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 1900 4300 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 1900 4300 50  0001 C CNN
	2    1900 4300
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U1
U 3 1 5BF494E9
P 1900 4650
F 0 "U1" H 1900 4967 50  0000 C CNN
F 1 "4069" H 1900 4876 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 1900 4650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 1900 4650 50  0001 C CNN
	3    1900 4650
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U1
U 7 1 5BF49536
P 9250 2550
F 0 "U1" H 9480 2596 50  0000 L CNN
F 1 "4069" H 9480 2505 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 9250 2550 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 9250 2550 50  0001 C CNN
	7    9250 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3950 1500 3950
Wire Wire Line
	1600 4300 1500 4300
Wire Wire Line
	1600 4650 1500 4650
$Comp
L power:GND #PWR03
U 1 1 5BF4CA40
P 9250 3150
F 0 "#PWR03" H 9250 2900 50  0001 C CNN
F 1 "GND" H 9255 2977 50  0000 C CNN
F 2 "" H 9250 3150 50  0001 C CNN
F 3 "" H 9250 3150 50  0001 C CNN
	1    9250 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5BF4CA47
P 8650 2550
F 0 "C1" H 8765 2596 50  0000 L CNN
F 1 "100nF" H 8765 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8688 2400 50  0001 C CNN
F 3 "~" H 8650 2550 50  0001 C CNN
	1    8650 2550
	1    0    0    -1  
$EndComp
Text GLabel 9250 1900 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	9250 1900 9250 2000
Wire Wire Line
	9250 2000 8650 2000
Wire Wire Line
	8650 2000 8650 2400
Connection ~ 9250 2000
Wire Wire Line
	9250 2000 9250 2050
Wire Wire Line
	9250 3050 9250 3150
Wire Wire Line
	8650 2700 8650 3050
Wire Wire Line
	8650 3050 9250 3050
Connection ~ 9250 3050
Wire Wire Line
	9250 4900 9250 4950
Wire Wire Line
	9250 3900 9250 4050
Wire Wire Line
	8650 4050 9250 4050
Connection ~ 9250 4050
Wire Wire Line
	9250 4050 9250 4100
Wire Wire Line
	8650 4950 9250 4950
Connection ~ 9250 4950
Wire Wire Line
	9250 4950 9250 5000
Wire Wire Line
	2200 4300 2600 4300
Wire Wire Line
	2200 4650 2400 4650
Wire Wire Line
	2400 4650 2400 4400
Wire Wire Line
	2400 4400 2600 4400
Wire Wire Line
	2200 3950 2400 3950
Wire Wire Line
	2400 3950 2400 4200
Wire Wire Line
	2400 4200 2600 4200
Wire Wire Line
	1600 3600 2500 3600
Wire Wire Line
	2500 3600 2500 4100
Wire Wire Line
	2500 4100 2600 4100
Text GLabel 2950 1850 0    50   Output ~ 0
V_out_LEM
Text GLabel 2950 2050 0    50   Output ~ 0
V_out_Turck
Wire Wire Line
	2950 1850 3100 1850
Wire Wire Line
	2950 2050 3100 2050
Wire Wire Line
	3100 2250 2800 2250
$Comp
L Mechanical:MountingHole MH1
U 1 1 5C068899
P 1050 950
F 0 "MH1" H 1150 996 50  0000 L CNN
F 1 "MountingHole" H 1150 905 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1050 950 50  0001 C CNN
F 3 "~" H 1050 950 50  0001 C CNN
	1    1050 950 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH2
U 1 1 5C06D387
P 1050 1150
F 0 "MH2" H 1150 1196 50  0000 L CNN
F 1 "MountingHole" H 1150 1105 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1050 1150 50  0001 C CNN
F 3 "~" H 1050 1150 50  0001 C CNN
	1    1050 1150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH3
U 1 1 5C06D3DD
P 1050 1350
F 0 "MH3" H 1150 1396 50  0000 L CNN
F 1 "MountingHole" H 1150 1305 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1050 1350 50  0001 C CNN
F 3 "~" H 1050 1350 50  0001 C CNN
	1    1050 1350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH4
U 1 1 5C06D411
P 1050 1550
F 0 "MH4" H 1150 1596 50  0000 L CNN
F 1 "MountingHole" H 1150 1505 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1050 1550 50  0001 C CNN
F 3 "~" H 1050 1550 50  0001 C CNN
	1    1050 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5C073630
P 4250 5050
F 0 "#PWR0102" H 4250 4800 50  0001 C CNN
F 1 "GND" H 4255 4877 50  0000 C CNN
F 2 "" H 4250 5050 50  0001 C CNN
F 3 "" H 4250 5050 50  0001 C CNN
	1    4250 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4700 3450 4700
Wire Wire Line
	3450 4700 3450 4800
$Comp
L 4xxx:4069 U1
U 4 1 5C09781B
P 2400 5900
F 0 "U1" H 2400 6217 50  0000 C CNN
F 1 "4069" H 2400 6126 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2400 5900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 2400 5900 50  0001 C CNN
	4    2400 5900
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U1
U 5 1 5C09788C
P 2400 6200
F 0 "U1" H 2400 6517 50  0000 C CNN
F 1 "4069" H 2400 6426 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2400 6200 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 2400 6200 50  0001 C CNN
	5    2400 6200
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U1
U 6 1 5C0978F7
P 2400 6500
F 0 "U1" H 2400 6817 50  0000 C CNN
F 1 "4069" H 2400 6726 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2400 6500 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 2400 6500 50  0001 C CNN
	6    2400 6500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5C098970
P 2050 6650
F 0 "#PWR0108" H 2050 6400 50  0001 C CNN
F 1 "GND" H 2055 6477 50  0000 C CNN
F 2 "" H 2050 6650 50  0001 C CNN
F 3 "" H 2050 6650 50  0001 C CNN
	1    2050 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 6200 2050 6200
Wire Wire Line
	2050 6200 2050 6500
Wire Wire Line
	2050 6500 2100 6500
Wire Wire Line
	2050 6500 2050 6650
Connection ~ 2050 6500
Wire Wire Line
	2050 6200 2050 5900
Wire Wire Line
	2050 5900 2100 5900
Connection ~ 2050 6200
$Comp
L 4xxx:4027 U3
U 2 1 5C095BB1
P 3550 6300
F 0 "U3" H 3550 6778 50  0000 C CNN
F 1 "4027" H 3550 6687 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 3550 6300 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4027bms.pdf" H 3550 6300 50  0001 C CNN
	2    3550 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5C095D12
P 3350 6700
F 0 "#PWR0109" H 3350 6450 50  0001 C CNN
F 1 "GND" H 3355 6527 50  0000 C CNN
F 2 "" H 3350 6700 50  0001 C CNN
F 3 "" H 3350 6700 50  0001 C CNN
	1    3350 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 6200 3200 6200
Wire Wire Line
	3200 6200 3200 6300
Wire Wire Line
	3200 6300 3250 6300
Wire Wire Line
	3200 6300 3200 6400
Wire Wire Line
	3200 6400 3250 6400
Connection ~ 3200 6300
Wire Wire Line
	3200 6400 3200 6700
Wire Wire Line
	3200 6700 3350 6700
Connection ~ 3200 6400
Wire Wire Line
	3550 6600 3550 6700
Wire Wire Line
	3550 6700 3350 6700
Connection ~ 3350 6700
Wire Wire Line
	3200 6200 3200 6000
Wire Wire Line
	3200 6000 3550 6000
Connection ~ 3200 6200
Wire Wire Line
	4100 4600 4550 4600
$Comp
L Device:R R?
U 1 1 5C1054CF
P 4550 4800
AR Path="/5BD8CA1D/5C1054CF" Ref="R?"  Part="1" 
AR Path="/5C1054CF" Ref="R35"  Part="1" 
F 0 "R35" H 4620 4846 50  0000 L CNN
F 1 "10k" H 4620 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4480 4800 50  0001 C CNN
F 3 "~" H 4550 4800 50  0001 C CNN
	1    4550 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4650 4550 4600
Connection ~ 4550 4600
Wire Wire Line
	4550 4600 4700 4600
$Comp
L 4xxx:4027 U3
U 1 1 5BD88D53
P 3800 4700
F 0 "U3" H 3800 5178 50  0000 C CNN
F 1 "4027" H 3800 5087 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 3800 4700 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4027bms.pdf" H 3800 4700 50  0001 C CNN
	1    3800 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5100 3800 5000
Wire Wire Line
	3800 4250 3800 4400
Wire Wire Line
	3200 4250 3800 4250
Wire Wire Line
	3500 4600 3450 4600
Wire Wire Line
	3450 4600 3450 4700
Connection ~ 3450 4700
Wire Wire Line
	3500 4800 3450 4800
Connection ~ 3450 4800
Wire Wire Line
	4550 5050 4250 5050
Wire Wire Line
	4550 4950 4550 5050
Wire Wire Line
	4250 5050 3450 5050
Wire Wire Line
	3450 4800 3450 5050
Connection ~ 4250 5050
Wire Wire Line
	2800 2350 3100 2350
$EndSCHEMATC
