EESchema Schematic File Version 4
LIBS:KICAD_Platine1.0-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title "Powersupply"
Date "2018-10-30"
Rev "V 1.0"
Comp "CURE Mannheim"
Comment1 ""
Comment2 "Maximilian Majeske"
Comment3 "Author: William Großmann"
Comment4 "Brake System Plausability Deveice"
$EndDescr
$Comp
L Regulator_Linear:L7805 U12
U 1 1 5BD8D3D8
P 5750 3750
F 0 "U12" H 5750 3992 50  0000 C CNN
F 1 "L7805" H 5750 3901 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 5775 3600 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 5750 3700 50  0001 C CNN
	1    5750 3750
	1    0    0    -1  
$EndComp
Text GLabel 4900 3750 0    50   Output ~ 0
Vcc_24V
Wire Wire Line
	4900 3750 5050 3750
Text GLabel 7200 3750 2    50   Input ~ 0
Vcc_5V
$Comp
L power:GND #PWR032
U 1 1 5BD8D3E3
P 5750 4250
F 0 "#PWR032" H 5750 4000 50  0001 C CNN
F 1 "GND" H 5755 4077 50  0000 C CNN
F 2 "" H 5750 4250 50  0001 C CNN
F 3 "" H 5750 4250 50  0001 C CNN
	1    5750 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4050 5750 4200
$Comp
L Diode:1N47xxA D2
U 1 1 5BD8D3EA
P 6300 3950
F 0 "D2" V 6254 4029 50  0000 L CNN
F 1 "1N47xxA" V 6345 4029 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 6300 3775 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 6300 3950 50  0001 C CNN
	1    6300 3950
	0    1    1    0   
$EndComp
Connection ~ 5750 4200
Wire Wire Line
	5750 4200 5750 4250
Text Notes 5850 4050 0    50   ~ 0
BZX55C\n
$Comp
L Device:C C14
U 1 1 5BD8D3F8
P 5050 4000
F 0 "C14" H 5165 4046 50  0000 L CNN
F 1 "100n" H 5165 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5088 3850 50  0001 C CNN
F 3 "~" H 5050 4000 50  0001 C CNN
	1    5050 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3850 5050 3750
Connection ~ 5050 3750
Wire Wire Line
	5050 4150 5050 4200
$Comp
L Device:C C15
U 1 1 5BDF1CA9
P 7050 4050
F 0 "C15" H 6935 4004 50  0000 R CNN
F 1 "100n" H 6935 4095 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7088 3900 50  0001 C CNN
F 3 "~" H 7050 4050 50  0001 C CNN
	1    7050 4050
	-1   0    0    1   
$EndComp
$Comp
L pspice:DIODE D1
U 1 1 5BDF1D32
P 5750 3300
F 0 "D1" H 5750 3035 50  0000 C CNN
F 1 "DIODE" H 5750 3126 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5750 3300 50  0001 C CNN
F 3 "" H 5750 3300 50  0001 C CNN
	1    5750 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	5050 3300 5050 3750
Wire Wire Line
	6050 3750 6300 3750
Wire Wire Line
	7050 3900 7050 3750
Connection ~ 7050 3750
Wire Wire Line
	7050 3750 7200 3750
Text Notes 7200 4200 0    50   ~ 0
entprelldiode\n
Text Notes 5900 3150 0    50   ~ 0
Ausgang darf nicht höher als eingangsseite sein
Wire Wire Line
	5750 4200 6300 4200
Wire Wire Line
	6300 3800 6300 3750
Connection ~ 6300 3750
Wire Wire Line
	6300 3750 7050 3750
Wire Wire Line
	6300 4100 6300 4200
Connection ~ 6300 4200
Wire Wire Line
	6300 4200 7050 4200
Wire Wire Line
	6300 3750 6300 3300
Wire Wire Line
	5950 3300 6300 3300
Wire Wire Line
	5050 3750 5450 3750
Wire Wire Line
	5050 3300 5550 3300
Wire Wire Line
	5050 4200 5750 4200
Text GLabel 7050 3600 0    50   Input ~ 0
V_out_LEM
Text GLabel 4900 3300 0    50   Input ~ 0
V_out_Turck
Wire Wire Line
	4900 3300 5050 3300
Connection ~ 5050 3300
Wire Wire Line
	7050 3600 7050 3750
$EndSCHEMATC
