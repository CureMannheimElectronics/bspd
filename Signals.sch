EESchema Schematic File Version 4
LIBS:KICAD_Platine1.0-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "Power_sensor_signal"
Date "2018-10-30"
Rev "V 1.0"
Comp "CURE Mannheim"
Comment1 ""
Comment2 "Maximilian Majeske"
Comment3 "Author: William Großmann"
Comment4 "Brake System Plausability Deveice"
$EndDescr
$Comp
L Comparator:LM2903 U5
U 1 1 5BD8D63A
P 5100 3050
F 0 "U5" H 5100 3417 50  0000 C CNN
F 1 "LM2903" H 5100 3326 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5100 3050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 5100 3050 50  0001 C CNN
	1    5100 3050
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U5
U 2 1 5BD8D711
P 5150 5000
F 0 "U5" H 5150 5367 50  0000 C CNN
F 1 "LM2903" H 5150 5276 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5150 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 5150 5000 50  0001 C CNN
	2    5150 5000
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U5
U 3 1 5BD8D7F2
P 9150 3700
F 0 "U5" H 9108 3746 50  0000 L CNN
F 1 "LM2903" H 9108 3655 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9150 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 9150 3700 50  0001 C CNN
	3    9150 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5BD8D936
P 4450 3400
F 0 "R4" H 4520 3446 50  0000 L CNN
F 1 "10k" H 4520 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4380 3400 50  0001 C CNN
F 3 "~" H 4450 3400 50  0001 C CNN
	1    4450 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5BD8DA56
P 4450 2700
F 0 "R3" H 4520 2746 50  0000 L CNN
F 1 "4.7k" H 4520 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4380 2700 50  0001 C CNN
F 3 "~" H 4450 2700 50  0001 C CNN
	1    4450 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BD8DA88
P 2900 3150
F 0 "R2" V 2693 3150 50  0000 C CNN
F 1 "100k" V 2784 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2830 3150 50  0001 C CNN
F 3 "~" H 2900 3150 50  0001 C CNN
	1    2900 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5BD8DB03
P 2500 3350
F 0 "R1" H 2430 3304 50  0000 R CNN
F 1 "10k" H 2430 3395 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2430 3350 50  0001 C CNN
F 3 "~" H 2500 3350 50  0001 C CNN
	1    2500 3350
	-1   0    0    1   
$EndComp
$Comp
L Device:C C3
U 1 1 5BD8DC0C
P 3300 3350
F 0 "C3" H 3415 3396 50  0000 L CNN
F 1 "10n" H 3415 3305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3338 3200 50  0001 C CNN
F 3 "~" H 3300 3350 50  0001 C CNN
	1    3300 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5BD8DDBC
P 3300 3550
F 0 "#PWR06" H 3300 3300 50  0001 C CNN
F 1 "GND" H 3305 3377 50  0000 C CNN
F 2 "" H 3300 3550 50  0001 C CNN
F 3 "" H 3300 3550 50  0001 C CNN
	1    3300 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5BD8DDDE
P 2500 3550
F 0 "#PWR05" H 2500 3300 50  0001 C CNN
F 1 "GND" H 2505 3377 50  0000 C CNN
F 2 "" H 2500 3550 50  0001 C CNN
F 3 "" H 2500 3550 50  0001 C CNN
	1    2500 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5BD8DE82
P 4450 3550
F 0 "#PWR08" H 4450 3300 50  0001 C CNN
F 1 "GND" H 4455 3377 50  0000 C CNN
F 2 "" H 4450 3550 50  0001 C CNN
F 3 "" H 4450 3550 50  0001 C CNN
	1    4450 3550
	1    0    0    -1  
$EndComp
Text GLabel 2200 3150 0    50   Output ~ 0
S_Power
Wire Wire Line
	4450 2350 4450 2550
Wire Wire Line
	3050 3150 3300 3150
Text GLabel 5550 3050 2    50   Input ~ 0
C_Power_high
Wire Wire Line
	5550 3050 5450 3050
Text GLabel 9050 3200 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR010
U 1 1 5BD8F03A
P 9050 4150
F 0 "#PWR010" H 9050 3900 50  0001 C CNN
F 1 "GND" H 9055 3977 50  0000 C CNN
F 2 "" H 9050 4150 50  0001 C CNN
F 3 "" H 9050 4150 50  0001 C CNN
	1    9050 4150
	1    0    0    -1  
$EndComp
Connection ~ 3300 3150
$Comp
L Device:R R6
U 1 1 5BD8F477
P 4450 5350
F 0 "R6" H 4520 5396 50  0000 L CNN
F 1 "8.2k" H 4520 5305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4380 5350 50  0001 C CNN
F 3 "~" H 4450 5350 50  0001 C CNN
	1    4450 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5BD8F47E
P 4450 4650
F 0 "R5" H 4520 4696 50  0000 L CNN
F 1 "10k" H 4520 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4380 4650 50  0001 C CNN
F 3 "~" H 4450 4650 50  0001 C CNN
	1    4450 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5BD8F48A
P 4450 5500
F 0 "#PWR09" H 4450 5250 50  0001 C CNN
F 1 "GND" H 4455 5327 50  0000 C CNN
F 2 "" H 4450 5500 50  0001 C CNN
F 3 "" H 4450 5500 50  0001 C CNN
	1    4450 5500
	1    0    0    -1  
$EndComp
Text GLabel 4450 4300 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	4450 4300 4450 4500
Wire Wire Line
	4100 3150 3650 3150
Wire Wire Line
	4450 4800 4450 5100
Wire Wire Line
	4100 4900 4850 4900
Wire Wire Line
	4850 5100 4450 5100
Connection ~ 4450 5100
Wire Wire Line
	4450 5100 4450 5200
Text GLabel 5550 5000 2    50   Input ~ 0
C_Powersensor_ok
Wire Wire Line
	5550 5000 5500 5000
Wire Wire Line
	9050 3200 9050 3350
Wire Wire Line
	9050 4000 9050 4050
$Comp
L Device:C C4
U 1 1 5BD90A1A
P 8650 3700
F 0 "C4" H 8765 3746 50  0000 L CNN
F 1 "100n" H 8765 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8688 3550 50  0001 C CNN
F 3 "~" H 8650 3700 50  0001 C CNN
	1    8650 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3550 8650 3350
Wire Wire Line
	8650 3350 9050 3350
Connection ~ 9050 3350
Wire Wire Line
	9050 3350 9050 3400
Wire Wire Line
	8650 3850 8650 4050
Wire Wire Line
	8650 4050 9050 4050
Connection ~ 9050 4050
Wire Wire Line
	9050 4050 9050 4150
Wire Wire Line
	2200 3150 2500 3150
Wire Wire Line
	2500 3150 2500 3200
Connection ~ 2500 3150
Wire Wire Line
	2500 3150 2750 3150
Wire Wire Line
	2500 3500 2500 3550
Wire Wire Line
	3300 3150 3300 3200
Wire Wire Line
	3300 3500 3300 3550
$Comp
L Diode:1N47xxA 10V?
U 1 1 5BD8A3E8
P 3650 3350
AR Path="/5BDABB9E/5BD8A3E8" Ref="10V?"  Part="1" 
AR Path="/5BD8CA1D/5BD8A3E8" Ref="10V1"  Part="1" 
F 0 "10V1" V 3604 3429 50  0000 L CNN
F 1 "1N47xxA" V 3695 3429 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3650 3175 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 3650 3350 50  0001 C CNN
	1    3650 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 3200 3650 3150
$Comp
L power:GND #PWR?
U 1 1 5BD8A3F2
P 3650 3550
AR Path="/5BDABB9E/5BD8A3F2" Ref="#PWR?"  Part="1" 
AR Path="/5BD8CA1D/5BD8A3F2" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 3650 3300 50  0001 C CNN
F 1 "GND" H 3655 3377 50  0000 C CNN
F 2 "" H 3650 3550 50  0001 C CNN
F 3 "" H 3650 3550 50  0001 C CNN
	1    3650 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3500 3650 3550
Connection ~ 3650 3150
Wire Wire Line
	3650 3150 3300 3150
Text GLabel 4450 2350 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	4450 2850 4450 2950
Wire Wire Line
	4100 4900 4100 3150
Wire Wire Line
	4100 3150 4800 3150
Connection ~ 4100 3150
Wire Wire Line
	4800 2950 4450 2950
Connection ~ 4450 2950
Wire Wire Line
	4450 2950 4450 3250
$Comp
L Device:R R7
U 1 1 5BEAFC05
P 5450 2700
F 0 "R7" H 5520 2746 50  0000 L CNN
F 1 "10k" H 5520 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5380 2700 50  0001 C CNN
F 3 "~" H 5450 2700 50  0001 C CNN
	1    5450 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5BEAFCBB
P 5500 4650
F 0 "R8" H 5570 4696 50  0000 L CNN
F 1 "10k" H 5570 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5430 4650 50  0001 C CNN
F 3 "~" H 5500 4650 50  0001 C CNN
	1    5500 4650
	1    0    0    -1  
$EndComp
Text GLabel 5450 2350 1    50   Output ~ 0
Vcc_5V
Text GLabel 5500 4300 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	5500 4300 5500 4500
Wire Wire Line
	5500 4800 5500 5000
Connection ~ 5500 5000
Wire Wire Line
	5500 5000 5450 5000
Wire Wire Line
	5450 2350 5450 2550
Wire Wire Line
	5450 2850 5450 3050
Connection ~ 5450 3050
Wire Wire Line
	5450 3050 5400 3050
$EndSCHEMATC
