EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Brake_Signal"
Date "2018-10-30"
Rev "V 1.0"
Comp "CURE"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Comparator:LM2903 U?
U 1 1 5BD9EFAA
P 5350 3000
AR Path="/5BD8CA1D/5BD9EFAA" Ref="U?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFAA" Ref="U?"  Part="1" 
F 0 "U?" H 5350 3367 50  0000 C CNN
F 1 "LM2903" H 5350 3276 50  0000 C CNN
F 2 "" H 5350 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 5350 3000 50  0001 C CNN
	1    5350 3000
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U?
U 2 1 5BD9EFB1
P 5400 4950
AR Path="/5BD8CA1D/5BD9EFB1" Ref="U?"  Part="2" 
AR Path="/5BD9EBF0/5BD9EFB1" Ref="U?"  Part="2" 
F 0 "U?" H 5400 5317 50  0000 C CNN
F 1 "LM2903" H 5400 5226 50  0000 C CNN
F 2 "" H 5400 4950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 5400 4950 50  0001 C CNN
	2    5400 4950
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U?
U 3 1 5BD9EFB8
P 9300 3350
AR Path="/5BD8CA1D/5BD9EFB8" Ref="U?"  Part="3" 
AR Path="/5BD9EBF0/5BD9EFB8" Ref="U?"  Part="3" 
F 0 "U?" H 9258 3396 50  0000 L CNN
F 1 "LM2903" H 9258 3305 50  0000 L CNN
F 2 "" H 9300 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 9300 3350 50  0001 C CNN
	3    9300 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5BD9EFBF
P 4700 3350
AR Path="/5BD8CA1D/5BD9EFBF" Ref="R?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFBF" Ref="R?"  Part="1" 
F 0 "R?" H 4770 3396 50  0000 L CNN
F 1 "100k" H 4770 3305 50  0000 L CNN
F 2 "" V 4630 3350 50  0001 C CNN
F 3 "~" H 4700 3350 50  0001 C CNN
	1    4700 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5BD9EFC6
P 4700 2650
AR Path="/5BD8CA1D/5BD9EFC6" Ref="R?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFC6" Ref="R?"  Part="1" 
F 0 "R?" H 4770 2696 50  0000 L CNN
F 1 "5k" H 4770 2605 50  0000 L CNN
F 2 "" V 4630 2650 50  0001 C CNN
F 3 "~" H 4700 2650 50  0001 C CNN
	1    4700 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5BD9EFCD
P 3150 2900
AR Path="/5BD8CA1D/5BD9EFCD" Ref="R?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFCD" Ref="R?"  Part="1" 
F 0 "R?" V 2943 2900 50  0000 C CNN
F 1 "1k" V 3034 2900 50  0000 C CNN
F 2 "" V 3080 2900 50  0001 C CNN
F 3 "~" H 3150 2900 50  0001 C CNN
	1    3150 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BD9EFD4
P 2750 3100
AR Path="/5BD8CA1D/5BD9EFD4" Ref="R?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFD4" Ref="R?"  Part="1" 
F 0 "R?" H 2680 3054 50  0000 R CNN
F 1 "1 k" H 2680 3145 50  0000 R CNN
F 2 "" V 2680 3100 50  0001 C CNN
F 3 "~" H 2750 3100 50  0001 C CNN
	1    2750 3100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5BD9EFDB
P 3550 3100
AR Path="/5BD8CA1D/5BD9EFDB" Ref="C?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFDB" Ref="C?"  Part="1" 
F 0 "C?" H 3665 3146 50  0000 L CNN
F 1 "10n" H 3665 3055 50  0000 L CNN
F 2 "" H 3588 2950 50  0001 C CNN
F 3 "~" H 3550 3100 50  0001 C CNN
	1    3550 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BD9EFE2
P 3550 3300
AR Path="/5BD8CA1D/5BD9EFE2" Ref="#PWR?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFE2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3550 3050 50  0001 C CNN
F 1 "GND" H 3555 3127 50  0000 C CNN
F 2 "" H 3550 3300 50  0001 C CNN
F 3 "" H 3550 3300 50  0001 C CNN
	1    3550 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BD9EFE8
P 2750 3300
AR Path="/5BD8CA1D/5BD9EFE8" Ref="#PWR?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFE8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2750 3050 50  0001 C CNN
F 1 "GND" H 2755 3127 50  0000 C CNN
F 2 "" H 2750 3300 50  0001 C CNN
F 3 "" H 2750 3300 50  0001 C CNN
	1    2750 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BD9EFEE
P 4700 3500
AR Path="/5BD8CA1D/5BD9EFEE" Ref="#PWR?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFEE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4700 3250 50  0001 C CNN
F 1 "GND" H 4705 3327 50  0000 C CNN
F 2 "" H 4700 3500 50  0001 C CNN
F 3 "" H 4700 3500 50  0001 C CNN
	1    4700 3500
	1    0    0    -1  
$EndComp
Text GLabel 2450 2900 0    50   Output ~ 0
S_Brake
Text GLabel 4700 2300 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	4700 2300 4700 2500
Wire Wire Line
	3300 2900 3550 2900
Text GLabel 5800 3000 2    50   Input ~ 0
C_Brake_high
Wire Wire Line
	5800 3000 5650 3000
Text GLabel 9200 2850 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR?
U 1 1 5BD9EFFB
P 9200 3800
AR Path="/5BD8CA1D/5BD9EFFB" Ref="#PWR?"  Part="1" 
AR Path="/5BD9EBF0/5BD9EFFB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9200 3550 50  0001 C CNN
F 1 "GND" H 9205 3627 50  0000 C CNN
F 2 "" H 9200 3800 50  0001 C CNN
F 3 "" H 9200 3800 50  0001 C CNN
	1    9200 3800
	1    0    0    -1  
$EndComp
Connection ~ 3550 2900
$Comp
L Device:R R?
U 1 1 5BD9F002
P 4700 5300
AR Path="/5BD8CA1D/5BD9F002" Ref="R?"  Part="1" 
AR Path="/5BD9EBF0/5BD9F002" Ref="R?"  Part="1" 
F 0 "R?" H 4770 5346 50  0000 L CNN
F 1 "100k" H 4770 5255 50  0000 L CNN
F 2 "" V 4630 5300 50  0001 C CNN
F 3 "~" H 4700 5300 50  0001 C CNN
	1    4700 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5BD9F009
P 4700 4600
AR Path="/5BD8CA1D/5BD9F009" Ref="R?"  Part="1" 
AR Path="/5BD9EBF0/5BD9F009" Ref="R?"  Part="1" 
F 0 "R?" H 4770 4646 50  0000 L CNN
F 1 "30k" H 4770 4555 50  0000 L CNN
F 2 "" V 4630 4600 50  0001 C CNN
F 3 "~" H 4700 4600 50  0001 C CNN
	1    4700 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BD9F010
P 4700 5450
AR Path="/5BD8CA1D/5BD9F010" Ref="#PWR?"  Part="1" 
AR Path="/5BD9EBF0/5BD9F010" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4700 5200 50  0001 C CNN
F 1 "GND" H 4705 5277 50  0000 C CNN
F 2 "" H 4700 5450 50  0001 C CNN
F 3 "" H 4700 5450 50  0001 C CNN
	1    4700 5450
	1    0    0    -1  
$EndComp
Text GLabel 4700 4250 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	4700 4250 4700 4450
Wire Wire Line
	4350 4850 4350 2900
Wire Wire Line
	4350 2900 3550 2900
Wire Wire Line
	4700 2800 4700 3100
Wire Wire Line
	4350 2900 5050 2900
Connection ~ 4350 2900
Wire Wire Line
	5050 3100 4700 3100
Connection ~ 4700 3100
Wire Wire Line
	4700 3100 4700 3200
Wire Wire Line
	4700 4750 4700 5050
Wire Wire Line
	4350 4850 5100 4850
Wire Wire Line
	5100 5050 4700 5050
Connection ~ 4700 5050
Wire Wire Line
	4700 5050 4700 5150
Text GLabel 5800 4950 2    50   Input ~ 0
C_Brake_ok
Wire Wire Line
	5800 4950 5700 4950
Wire Wire Line
	9200 2850 9200 3000
Wire Wire Line
	9200 3650 9200 3700
$Comp
L Device:C C?
U 1 1 5BD9F029
P 8800 3350
AR Path="/5BD8CA1D/5BD9F029" Ref="C?"  Part="1" 
AR Path="/5BD9EBF0/5BD9F029" Ref="C?"  Part="1" 
F 0 "C?" H 8915 3396 50  0000 L CNN
F 1 "C" H 8915 3305 50  0000 L CNN
F 2 "" H 8838 3200 50  0001 C CNN
F 3 "~" H 8800 3350 50  0001 C CNN
	1    8800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3200 8800 3000
Wire Wire Line
	8800 3000 9200 3000
Connection ~ 9200 3000
Wire Wire Line
	9200 3000 9200 3050
Wire Wire Line
	8800 3500 8800 3700
Wire Wire Line
	8800 3700 9200 3700
Connection ~ 9200 3700
Wire Wire Line
	9200 3700 9200 3800
Wire Wire Line
	2450 2900 2750 2900
Wire Wire Line
	2750 2900 2750 2950
Connection ~ 2750 2900
Wire Wire Line
	2750 2900 3000 2900
Wire Wire Line
	2750 3250 2750 3300
Wire Wire Line
	3550 2900 3550 2950
Wire Wire Line
	3550 3250 3550 3300
$EndSCHEMATC
