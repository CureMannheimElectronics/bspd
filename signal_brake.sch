EESchema Schematic File Version 4
LIBS:KICAD_Platine1.0-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "brake signal"
Date "2018-10-30"
Rev "V 1.0"
Comp "CURE Mannheim"
Comment1 ""
Comment2 "Maximilian Majeske"
Comment3 "Author: William Großmann"
Comment4 "Brake System Plausability Deveice"
$EndDescr
$Comp
L Comparator:LM2903 U7
U 1 1 5BDABE66
P 4600 2800
F 0 "U7" H 4600 3167 50  0000 C CNN
F 1 "LM2903" H 4600 3076 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4600 2800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 4600 2800 50  0001 C CNN
	1    4600 2800
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U7
U 2 1 5BDABE6D
P 6400 1500
F 0 "U7" H 6400 1867 50  0000 C CNN
F 1 "LM2903" H 6400 1776 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6400 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 6400 1500 50  0001 C CNN
	2    6400 1500
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U7
U 3 1 5BDABE74
P 10500 4550
F 0 "U7" H 10458 4596 50  0000 L CNN
F 1 "LM2903" H 10458 4505 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 10500 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 10500 4550 50  0001 C CNN
	3    10500 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5BDABE89
P 2350 2900
F 0 "R12" V 2143 2900 50  0000 C CNN
F 1 "100k" V 2234 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2280 2900 50  0001 C CNN
F 3 "~" H 2350 2900 50  0001 C CNN
	1    2350 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BDABE90
P 1950 3100
AR Path="/5BD8CA1D/5BDABE90" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BDABE90" Ref="R10"  Part="1" 
F 0 "R10" H 1880 3054 50  0000 R CNN
F 1 "270" H 1880 3145 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1880 3100 50  0001 C CNN
F 3 "~" H 1950 3100 50  0001 C CNN
	1    1950 3100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C6
U 1 1 5BDABE97
P 2750 3100
F 0 "C6" H 2865 3146 50  0000 L CNN
F 1 "10n" H 2865 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2788 2950 50  0001 C CNN
F 3 "~" H 2750 3100 50  0001 C CNN
	1    2750 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5BDABE9E
P 2750 3300
F 0 "#PWR014" H 2750 3050 50  0001 C CNN
F 1 "GND" H 2755 3127 50  0000 C CNN
F 2 "" H 2750 3300 50  0001 C CNN
F 3 "" H 2750 3300 50  0001 C CNN
	1    2750 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5BDABEA4
P 1950 3300
F 0 "#PWR012" H 1950 3050 50  0001 C CNN
F 1 "GND" H 1955 3127 50  0000 C CNN
F 2 "" H 1950 3300 50  0001 C CNN
F 3 "" H 1950 3300 50  0001 C CNN
	1    1950 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5BDABEAA
P 3950 3300
F 0 "#PWR018" H 3950 3050 50  0001 C CNN
F 1 "GND" H 3955 3127 50  0000 C CNN
F 2 "" H 3950 3300 50  0001 C CNN
F 3 "" H 3950 3300 50  0001 C CNN
	1    3950 3300
	1    0    0    -1  
$EndComp
Text GLabel 1650 2900 0    50   Output ~ 0
S_Brake1
Text GLabel 3950 2050 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	2500 2900 2750 2900
Text GLabel 6650 3850 2    50   Input ~ 0
C_Brake_high
Text GLabel 10400 4050 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR022
U 1 1 5BDABEB7
P 10400 5000
F 0 "#PWR022" H 10400 4750 50  0001 C CNN
F 1 "GND" H 10405 4827 50  0000 C CNN
F 2 "" H 10400 5000 50  0001 C CNN
F 3 "" H 10400 5000 50  0001 C CNN
	1    10400 5000
	1    0    0    -1  
$EndComp
Connection ~ 2750 2900
$Comp
L Device:R R23
U 1 1 5BDABEBE
P 5700 1850
F 0 "R23" H 5770 1896 50  0000 L CNN
F 1 "2.2k" H 5770 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5630 1850 50  0001 C CNN
F 3 "~" H 5700 1850 50  0001 C CNN
	1    5700 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5BDABEC5
P 5700 1150
AR Path="/5BD8CA1D/5BDABEC5" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BDABEC5" Ref="R22"  Part="1" 
F 0 "R22" H 5770 1196 50  0000 L CNN
F 1 "10k" H 5770 1105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5630 1150 50  0001 C CNN
F 3 "~" H 5700 1150 50  0001 C CNN
	1    5700 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5BDABECC
P 5700 2000
F 0 "#PWR020" H 5700 1750 50  0001 C CNN
F 1 "GND" H 5705 1827 50  0000 C CNN
F 2 "" H 5700 2000 50  0001 C CNN
F 3 "" H 5700 2000 50  0001 C CNN
	1    5700 2000
	1    0    0    -1  
$EndComp
Text GLabel 5700 900  1    50   Output ~ 0
Vcc_5V
Text GLabel 6800 1500 2    50   Input ~ 0
C_Brakesensor1_ok
Wire Wire Line
	6800 1500 6750 1500
Wire Wire Line
	10400 4050 10400 4200
Wire Wire Line
	10400 4850 10400 4900
$Comp
L Device:C C8
U 1 1 5BDABEE5
P 10000 4550
F 0 "C8" H 10115 4596 50  0000 L CNN
F 1 "100nF" H 10115 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10038 4400 50  0001 C CNN
F 3 "~" H 10000 4550 50  0001 C CNN
	1    10000 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4400 10000 4200
Wire Wire Line
	10000 4200 10400 4200
Connection ~ 10400 4200
Wire Wire Line
	10400 4200 10400 4250
Wire Wire Line
	10000 4700 10000 4900
Wire Wire Line
	10000 4900 10400 4900
Connection ~ 10400 4900
Wire Wire Line
	10400 4900 10400 5000
Wire Wire Line
	1650 2900 1950 2900
Wire Wire Line
	1950 2900 1950 2950
Connection ~ 1950 2900
Wire Wire Line
	1950 2900 2200 2900
Wire Wire Line
	1950 3250 1950 3300
Wire Wire Line
	2750 2900 2750 2950
Wire Wire Line
	2750 3250 2750 3300
$Comp
L Diode:1N47xxA 10V3
U 1 1 5BD89A06
P 3100 3100
F 0 "10V3" V 3054 3179 50  0000 L CNN
F 1 "1N47xxA" V 3145 3179 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3100 2925 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 3100 3100 50  0001 C CNN
	1    3100 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 2950 3100 2900
$Comp
L power:GND #PWR016
U 1 1 5BD89E41
P 3100 3300
F 0 "#PWR016" H 3100 3050 50  0001 C CNN
F 1 "GND" H 3105 3127 50  0000 C CNN
F 2 "" H 3100 3300 50  0001 C CNN
F 3 "" H 3100 3300 50  0001 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3250 3100 3300
Connection ~ 3100 2900
Wire Wire Line
	3100 2900 2750 2900
Wire Wire Line
	5700 1300 5700 1600
Wire Wire Line
	6100 1600 5700 1600
Connection ~ 5700 1600
Wire Wire Line
	5700 1600 5700 1700
$Comp
L Device:R R?
U 1 1 5BEB145A
P 4900 2400
AR Path="/5BD8CA1D/5BEB145A" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BEB145A" Ref="R20"  Part="1" 
F 0 "R20" H 4970 2446 50  0000 L CNN
F 1 "10k" H 4970 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4830 2400 50  0001 C CNN
F 3 "~" H 4900 2400 50  0001 C CNN
	1    4900 2400
	1    0    0    -1  
$EndComp
Text GLabel 4900 2050 1    50   Output ~ 0
Vcc_5V
$Comp
L Device:R R?
U 1 1 5BEB14E8
P 6750 1150
AR Path="/5BD8CA1D/5BEB14E8" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BEB14E8" Ref="R24"  Part="1" 
F 0 "R24" H 6820 1196 50  0000 L CNN
F 1 "10k" H 6820 1105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6680 1150 50  0001 C CNN
F 3 "~" H 6750 1150 50  0001 C CNN
	1    6750 1150
	1    0    0    -1  
$EndComp
Text GLabel 6750 900  1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	6750 1300 6750 1500
Connection ~ 6750 1500
Wire Wire Line
	6750 1500 6700 1500
Wire Wire Line
	4900 2050 4900 2250
Wire Wire Line
	4900 2550 4900 2800
$Comp
L Comparator:LM2903 U6
U 1 1 5BEB9544
P 4550 4900
F 0 "U6" H 4550 5267 50  0000 C CNN
F 1 "LM2903" H 4550 5176 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4550 4900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 4550 4900 50  0001 C CNN
	1    4550 4900
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U6
U 2 1 5BEB954B
P 5200 6750
F 0 "U6" H 5200 7117 50  0000 C CNN
F 1 "LM2903" H 5200 7026 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5200 6750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 5200 6750 50  0001 C CNN
	2    5200 6750
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U6
U 3 1 5BEB9552
P 10500 2750
F 0 "U6" H 10458 2796 50  0000 L CNN
F 1 "LM2903" H 10458 2705 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 10500 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 10500 2750 50  0001 C CNN
	3    10500 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5BEB9567
P 2300 5000
F 0 "R11" V 2093 5000 50  0000 C CNN
F 1 "100k" V 2184 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2230 5000 50  0001 C CNN
F 3 "~" H 2300 5000 50  0001 C CNN
	1    2300 5000
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BEB956E
P 1900 5200
AR Path="/5BD8CA1D/5BEB956E" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BEB956E" Ref="R9"  Part="1" 
F 0 "R9" H 1830 5154 50  0000 R CNN
F 1 "270" H 1830 5245 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1830 5200 50  0001 C CNN
F 3 "~" H 1900 5200 50  0001 C CNN
	1    1900 5200
	-1   0    0    1   
$EndComp
$Comp
L Device:C C5
U 1 1 5BEB9575
P 2700 5200
F 0 "C5" H 2815 5246 50  0000 L CNN
F 1 "10n" H 2815 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2738 5050 50  0001 C CNN
F 3 "~" H 2700 5200 50  0001 C CNN
	1    2700 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5BEB957C
P 2700 5400
F 0 "#PWR013" H 2700 5150 50  0001 C CNN
F 1 "GND" H 2705 5227 50  0000 C CNN
F 2 "" H 2700 5400 50  0001 C CNN
F 3 "" H 2700 5400 50  0001 C CNN
	1    2700 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5BEB9582
P 1900 5400
F 0 "#PWR011" H 1900 5150 50  0001 C CNN
F 1 "GND" H 1905 5227 50  0000 C CNN
F 2 "" H 1900 5400 50  0001 C CNN
F 3 "" H 1900 5400 50  0001 C CNN
	1    1900 5400
	1    0    0    -1  
$EndComp
Text GLabel 1600 5000 0    50   Output ~ 0
S_Brake2
Wire Wire Line
	2450 5000 2700 5000
Text GLabel 10400 2250 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR021
U 1 1 5BEB9595
P 10400 3200
F 0 "#PWR021" H 10400 2950 50  0001 C CNN
F 1 "GND" H 10405 3027 50  0000 C CNN
F 2 "" H 10400 3200 50  0001 C CNN
F 3 "" H 10400 3200 50  0001 C CNN
	1    10400 3200
	1    0    0    -1  
$EndComp
Connection ~ 2700 5000
$Comp
L Device:R R18
U 1 1 5BEB959C
P 4500 7100
F 0 "R18" H 4570 7146 50  0000 L CNN
F 1 "2.2k" H 4570 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 7100 50  0001 C CNN
F 3 "~" H 4500 7100 50  0001 C CNN
	1    4500 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5BEB95A3
P 4500 6400
AR Path="/5BD8CA1D/5BEB95A3" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BEB95A3" Ref="R17"  Part="1" 
F 0 "R17" H 4570 6446 50  0000 L CNN
F 1 "10k" H 4570 6355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 6400 50  0001 C CNN
F 3 "~" H 4500 6400 50  0001 C CNN
	1    4500 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5BEB95AA
P 4500 7250
F 0 "#PWR019" H 4500 7000 50  0001 C CNN
F 1 "GND" H 4505 7077 50  0000 C CNN
F 2 "" H 4500 7250 50  0001 C CNN
F 3 "" H 4500 7250 50  0001 C CNN
	1    4500 7250
	1    0    0    -1  
$EndComp
Text GLabel 4500 6050 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	4500 6050 4500 6250
Text GLabel 5600 6750 2    50   Input ~ 0
C_Brakesensor2_ok
Wire Wire Line
	5600 6750 5550 6750
Wire Wire Line
	10400 2250 10400 2400
Wire Wire Line
	10400 3050 10400 3100
$Comp
L Device:C C7
U 1 1 5BEB95B6
P 10000 2750
F 0 "C7" H 10115 2796 50  0000 L CNN
F 1 "100nF" H 10115 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10038 2600 50  0001 C CNN
F 3 "~" H 10000 2750 50  0001 C CNN
	1    10000 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 2600 10000 2400
Wire Wire Line
	10000 2400 10400 2400
Connection ~ 10400 2400
Wire Wire Line
	10400 2400 10400 2450
Wire Wire Line
	10000 2900 10000 3100
Wire Wire Line
	10000 3100 10400 3100
Connection ~ 10400 3100
Wire Wire Line
	10400 3100 10400 3200
Wire Wire Line
	1600 5000 1900 5000
Wire Wire Line
	1900 5000 1900 5050
Connection ~ 1900 5000
Wire Wire Line
	1900 5000 2150 5000
Wire Wire Line
	1900 5350 1900 5400
Wire Wire Line
	2700 5000 2700 5050
Wire Wire Line
	2700 5350 2700 5400
Text Notes 700  4150 0    50   ~ 0
! Belastung des Sensors max. 750 Ohm -> Mosfet in Source Schaltung\n
$Comp
L Diode:1N47xxA 10V2
U 1 1 5BEB95CD
P 3050 5200
F 0 "10V2" V 3004 5279 50  0000 L CNN
F 1 "1N47xxA" V 3095 5279 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3050 5025 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 3050 5200 50  0001 C CNN
	1    3050 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	3050 5050 3050 5000
$Comp
L power:GND #PWR015
U 1 1 5BEB95D5
P 3050 5400
F 0 "#PWR015" H 3050 5150 50  0001 C CNN
F 1 "GND" H 3055 5227 50  0000 C CNN
F 2 "" H 3050 5400 50  0001 C CNN
F 3 "" H 3050 5400 50  0001 C CNN
	1    3050 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 5350 3050 5400
Connection ~ 3050 5000
Wire Wire Line
	3050 5000 2700 5000
Wire Wire Line
	4500 6550 4500 6850
Wire Wire Line
	4900 6850 4500 6850
Connection ~ 4500 6850
Wire Wire Line
	4500 6850 4500 6950
$Comp
L Device:R R?
U 1 1 5BEB95E6
P 4850 4500
AR Path="/5BD8CA1D/5BEB95E6" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BEB95E6" Ref="R19"  Part="1" 
F 0 "R19" H 4920 4546 50  0000 L CNN
F 1 "10k" H 4920 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4780 4500 50  0001 C CNN
F 3 "~" H 4850 4500 50  0001 C CNN
	1    4850 4500
	1    0    0    -1  
$EndComp
Text GLabel 4850 4150 1    50   Output ~ 0
Vcc_5V
$Comp
L Device:R R?
U 1 1 5BEB95EE
P 5550 6400
AR Path="/5BD8CA1D/5BEB95EE" Ref="R?"  Part="1" 
AR Path="/5BDABB9E/5BEB95EE" Ref="R21"  Part="1" 
F 0 "R21" H 5620 6446 50  0000 L CNN
F 1 "10k" H 5620 6355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5480 6400 50  0001 C CNN
F 3 "~" H 5550 6400 50  0001 C CNN
	1    5550 6400
	1    0    0    -1  
$EndComp
Text GLabel 5550 6050 1    50   Output ~ 0
Vcc_5V
Wire Wire Line
	5550 6050 5550 6250
Wire Wire Line
	5550 6550 5550 6750
Connection ~ 5550 6750
Wire Wire Line
	5550 6750 5500 6750
Wire Wire Line
	4850 4150 4850 4350
Wire Wire Line
	4850 4650 4850 4900
$Comp
L 4xxx:4072 U8
U 2 1 5BEBF769
P 6200 3850
F 0 "U8" H 6200 4225 50  0000 C CNN
F 1 "4072" H 6200 4134 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6200 3850 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 6200 3850 50  0001 C CNN
	2    6200 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3800 5900 3800
Wire Wire Line
	5650 3900 5900 3900
Wire Wire Line
	4900 6650 3650 6650
Wire Wire Line
	4900 2800 5650 2800
Wire Wire Line
	5650 2800 5650 3800
Connection ~ 4900 2800
Wire Wire Line
	4850 4900 5650 4900
Wire Wire Line
	5650 4900 5650 3900
Connection ~ 4850 4900
Wire Wire Line
	3100 2900 3650 2900
Wire Wire Line
	6100 1400 3650 1400
Wire Wire Line
	3650 1400 3650 2900
Connection ~ 3650 2900
Wire Wire Line
	3650 2900 4300 2900
Wire Wire Line
	6750 900  6750 1000
Wire Wire Line
	5700 900  5700 1000
Wire Wire Line
	6500 3850 6650 3850
Text Notes 1700 3650 0    50   ~ 0
Last 250 Ohm\n
$Comp
L 4xxx:4072 U8
U 3 1 5BF59409
P 9250 4550
F 0 "U8" H 9480 4596 50  0000 L CNN
F 1 "4072" H 9480 4505 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 9250 4550 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 9250 4550 50  0001 C CNN
	3    9250 4550
	1    0    0    -1  
$EndComp
Text GLabel 9250 4000 1    50   Output ~ 0
Vcc_5V
$Comp
L power:GND #PWR0101
U 1 1 5BF595B1
P 9250 5100
F 0 "#PWR0101" H 9250 4850 50  0001 C CNN
F 1 "GND" H 9255 4927 50  0000 C CNN
F 2 "" H 9250 5100 50  0001 C CNN
F 3 "" H 9250 5100 50  0001 C CNN
	1    9250 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5BF595F8
P 8700 4600
F 0 "C16" H 8815 4646 50  0000 L CNN
F 1 "100nF" H 8815 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8738 4450 50  0001 C CNN
F 3 "~" H 8700 4600 50  0001 C CNN
	1    8700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 4050 9250 4000
Wire Wire Line
	9250 4050 8700 4050
Wire Wire Line
	8700 4050 8700 4450
Connection ~ 9250 4050
Wire Wire Line
	9250 5100 9250 5050
Wire Wire Line
	9250 5050 8700 5050
Wire Wire Line
	8700 5050 8700 4750
Connection ~ 9250 5050
$Comp
L power:GND #PWR0103
U 1 1 5C07B141
P 5800 4150
F 0 "#PWR0103" H 5800 3900 50  0001 C CNN
F 1 "GND" H 5805 3977 50  0000 C CNN
F 2 "" H 5800 4150 50  0001 C CNN
F 3 "" H 5800 4150 50  0001 C CNN
	1    5800 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4150 5800 4000
Wire Wire Line
	5800 3700 5900 3700
Wire Wire Line
	5800 4000 5900 4000
Connection ~ 5800 4000
Wire Wire Line
	5800 4000 5800 3700
$Comp
L 4xxx:4072 U8
U 1 1 5C07FB0D
P 6200 3000
F 0 "U8" H 6200 3375 50  0000 C CNN
F 1 "4072" H 6200 3284 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6200 3000 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 6200 3000 50  0001 C CNN
	1    6200 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5C08214C
P 5800 3350
F 0 "#PWR0105" H 5800 3100 50  0001 C CNN
F 1 "GND" H 5805 3177 50  0000 C CNN
F 2 "" H 5800 3350 50  0001 C CNN
F 3 "" H 5800 3350 50  0001 C CNN
	1    5800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2850 5800 2850
Wire Wire Line
	5800 2850 5800 2950
Wire Wire Line
	5900 3150 5800 3150
Connection ~ 5800 3150
Wire Wire Line
	5800 3150 5800 3350
Wire Wire Line
	5900 3050 5800 3050
Connection ~ 5800 3050
Wire Wire Line
	5800 3050 5800 3150
Wire Wire Line
	5900 2950 5800 2950
Connection ~ 5800 2950
Wire Wire Line
	5800 2950 5800 3050
$Comp
L Device:R_POT_TRIM RV2
U 1 1 5C11174F
P 3950 2700
F 0 "RV2" H 3880 2746 50  0000 R CNN
F 1 "R_POT_TRIM" H 3880 2655 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296X_Horizontal" H 3950 2700 50  0001 C CNN
F 3 "~" H 3950 2700 50  0001 C CNN
	1    3950 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2550 3950 2050
Wire Wire Line
	3950 2850 3950 3300
Wire Wire Line
	4100 2700 4300 2700
$Comp
L power:GND #PWR017
U 1 1 5C1279DA
P 3900 5400
F 0 "#PWR017" H 3900 5150 50  0001 C CNN
F 1 "GND" H 3905 5227 50  0000 C CNN
F 2 "" H 3900 5400 50  0001 C CNN
F 3 "" H 3900 5400 50  0001 C CNN
	1    3900 5400
	1    0    0    -1  
$EndComp
Text GLabel 3900 4150 1    50   Output ~ 0
Vcc_5V
$Comp
L Device:R_POT_TRIM RV1
U 1 1 5C1279E2
P 3900 4800
F 0 "RV1" H 3830 4846 50  0000 R CNN
F 1 "R_POT_TRIM" H 3830 4755 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296X_Horizontal" H 3900 4800 50  0001 C CNN
F 3 "~" H 3900 4800 50  0001 C CNN
	1    3900 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4650 3900 4150
Wire Wire Line
	3900 4950 3900 5400
Wire Wire Line
	4050 4800 4250 4800
Wire Wire Line
	3050 5000 3650 5000
Wire Wire Line
	3650 6650 3650 5000
Connection ~ 3650 5000
Wire Wire Line
	3650 5000 4250 5000
$EndSCHEMATC
